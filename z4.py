import requests
import collections
import matplotlib.pyplot as plt
import numpy as np
class Pochta:
    pass
    
class authors(Pochta):
    def __init__(self):
        self.authorss=[]
        self.au=None
    def add_auth(self):
        self.authorss.append(self.au)
        
    def otpraviteli(self):
        povtor=dict(collections.Counter(self.authorss))
        return(povtor.keys(),povtor.values())
    
class message(Pochta):
    def __init__(self, result,auth,probability):
        self.result=result
        self.probability= probability
        self.auth=auth

def download(path,method):
    spisokvseh=[]
    X_DSPAM_Result=""
    X_DSPAM_Probability=""
    Author=""
    auth=authors()
    if method==1:
        file=(requests.get(path).text).splitlines()
    if method==0:
        file=open(path)
    for lines in file:
        if lines.startswith("X-DSPAM-Result:"):
            X_DSPAM_Result=(lines.split("X-DSPAM-Result: ")[1])
        if lines.startswith("X-DSPAM-Probability:"):
            X_DSPAM_Probability=(lines.split("X-DSPAM-Probability: ")[1])
        if lines.startswith("Author:"):
            Author=(lines.split("Author: ")[1])
            polzov=message(X_DSPAM_Result,Author,X_DSPAM_Probability)
            auth.au=Author
            auth.add_auth()
            spisokvseh.append(polzov)
    if method==0:
        file.close()
    return(spisokvseh,auth.otpraviteli())  
    
def srznach(spisokvseh):
    sr=0
    for i in range(len(spisokvseh)):
       sr+= (float(spisokvseh[i].probability))
    return(sr/len(spisokvseh))

def spamers(spisokspamers):
    flag=0
    for i in range(len(spisokspamers)):
        if spisokspamers[i].result.rstrip()!="Innocent":
            flag=1
            print("spamer")
            print(spisokspamers[i].auth)            
    if flag==0:
        print("no spamers")
    
def graph(grx,gry):
    position = np.arange(len(grx))
    fig, ax = plt.subplots()
    ax.barh(position, gry)
    ax.set_yticks(position)
    labels = ax.set_yticklabels(grx,fontsize = 8)
    plt.grid()
    plt.ylabel('email')
    plt.xlabel('количество сообщений, шт')
    plt.show()


def main(param,met):
    spsk,x=download(param,met)
    spamers(spsk)
    grx,gry=(x)
    graph(grx,gry)
    return(srznach(spsk))

##print("среднее X-DSPAM-Probability: "+str(main('mbox.txt',0)))
##main('http://www.py4e.com/code3/mbox.txt',1)
